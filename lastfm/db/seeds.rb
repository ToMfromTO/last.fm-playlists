# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

###
# TRACKS & TAGS
# track = Track.create( title: 'Wish You Were Here', artist_name: 'Pink Floyd' )
# tags = Tag.create([{ name: 'Rock' }, { name: 'Pink Floyd' }])
# track.tags = tags

###
# USERS
playlist = User.create( username: 'Tomas', password: '1234' )#.playlists.create( title: 'Awesome playlist', description: 'Playlist full of awesome tracks')
# playlist_track = playlist.playlist_tracks.create(order: 1, track_id: track.id)

# gabriela = User.create( username: 'Gabriela', password: '1234' )
# share = playlist.user_shares.create( :user_from_id => 1, :user_to_id => 2)

###
# PLAYLISTS & TRACKS
tracks = Track.create([  
    { title: 'For Whom the Bell Tolls', artist_name: 'Metallica' },
    { title: 'For Whom the Bell Tolls', artist_name: 'Metallica' },
    { title: 'Master of Puppets', artist_name: 'Metallica' },
    { title: 'Orion', artist_name: 'Metallica' },
    { title: 'One', artist_name: 'Metallica' },
    { title: 'Enter Sandman', artist_name: 'Metallica' },
    { title: 'The Unforgiven', artist_name: 'Metallica' },
    { title: 'Nothing Else Matters', artist_name: 'Metallica' },
    { title: 'Mama Said', artist_name: 'Metallica' },
    { title: 'Fuel', artist_name: 'Metallica' },
    { title: 'The Memory Remains', artist_name: 'Metallica' },
    { title: 'The Unforgiven II', artist_name: 'Metallica' },
    { title: 'The End Of The Line', artist_name: 'Metallica' },
    { title: 'Broken, Beat & Scarred', artist_name: 'Metallica' },
    { title: 'The Day That Never Comes', artist_name: 'Metallica' },
    { title: 'Cyanide"', artist_name: 'Metallica' },
    { title: 'The Unforgiven III', artist_name: 'Metallica' },
    { title: 'The Judas Kiss', artist_name: 'Metallica' }
                    ])

playlist = Playlist.create( user_id: 2, title: 'Metallica Mix', description: 'Rock & Roll baby!')

tags = Tag.create([{ name: 'metal' }, { name: 'pop' }])

tracks.each do |track|
    playlist.playlist_tracks.create(order: tracks.index(track) + 1, track_id: track.id)
    track.tags << tags
end

# playlist.user_shares.create(user_from_id: 2, user_to_id: 1)

# playlist2 = Playlist.create( user_id: 2, title: 'Other Playlist', description: 'Whatever!')

# playlist2.user_shares.create(user_from_id: 2, user_to_id: 1)

