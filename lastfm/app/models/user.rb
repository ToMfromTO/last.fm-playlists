class User < ActiveRecord::Base
    has_many :playlists, :dependent => :destroy

    has_many :playlist_shares, :class_name => 'PlaylistShare', :foreign_key => 'user_to_id', :dependent => :destroy
    has_many :shared_playlists, :class_name => 'PlaylistShare', :foreign_key => 'user_from_id', :dependent => :destroy

    # has_many :playlist_shares#, :dependent => :destroy
    # has_many :shared_playlists, :through => :playlist_shares, :source => :playlist

    validates :username, presence: true
    validates :password, presence: true
end
