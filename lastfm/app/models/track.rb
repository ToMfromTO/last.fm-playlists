class Track < ActiveRecord::Base
    has_and_belongs_to_many :tags

    has_many :playlist_tracks, :dependent => :destroy
    has_many :playlists, :through => :playlist_tracks, :source => :playlist

    validates :title, presence: true
    validates :artist_name, presence: true
end
