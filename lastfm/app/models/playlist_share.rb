class PlaylistShare < ActiveRecord::Base
    belongs_to :playlist
    belongs_to :user_from, :class_name => 'User'
    belongs_to :user_to, :class_name => 'User'

    validates :user_from_id, presence: true
    validates :user_to_id, presence: true
    validates :playlist_id, presence: true
end
