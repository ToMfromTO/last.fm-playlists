class PlaylistTrack < ActiveRecord::Base
    belongs_to :playlist
    belongs_to :track

    validates :order, presence: true
end
