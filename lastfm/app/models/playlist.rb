class Playlist < ActiveRecord::Base
    belongs_to :user
    
    # has_many :playlist_shares, :dependent => :destroy
    # has_many :user_shares, :through => :playlist_shares, :source => :user
    has_many :user_shares, :class_name => 'PlaylistShare', :foreign_key => 'playlist_id', :dependent => :destroy

    has_many :playlist_tracks, :dependent => :destroy
    has_many :tracks, :through => :playlist_tracks, :source => :track

    validates :title, presence: true
    validates :user_id, presence: true
end
