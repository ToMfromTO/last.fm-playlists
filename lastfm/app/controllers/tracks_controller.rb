class TracksController < ApplicationController
  before_action :set_track, only: [:show, :edit, :update, :destroy]
  before_action :set_playlist, only: [:create, :destroy, :index]
  before_action :authenticate, except: [:index, :show]
  skip_before_action :verify_authenticity_token
  wrap_parameters format: [:json]

  # GET (/playlists/:playlist_id)/tracks
  # GET (/playlists/:playlist_id)/tracks.json
  def index
    @tracks = Track.all
    if @playlist
      # @tracks = Track.includes(:playlists).where(playlists: {id: @playlist.id})
      respond_to do |format|
        format.html { render :index_playlist}
        format.json { render :index_playlist}
      end
    end
  end

  def index_playlist
    respond_to do |format|
      format.html { render html: @playlist.playlist_tracks}
      format.json { render json: @playlist.playlist_tracks, status: :ok }
    end
  end

  # GET /tracks/1
  # GET /tracks/1.json
  def show
  end

  # POST (/playlists/:playlist_id)/tracks
  # POST (/playlists/:playlist_id)/tracks.json
  def create
    if @playlist
      if track_params[:id]
        add_track_to_playlist
      else
        respond_to do |format|
          format.html { redirect_to @playlist, notice: 'Failed to add the track to the playlist. You must specify id of the track.' }
          format.json { render json: @playlist, status: :unprocessable_entity }
        end
      end
    else
      create_new_track
    end
  end

  def add_track_to_playlist
    @playlist.playlist_tracks.create(order: @playlist.playlist_tracks.count + 1, track_id: track_params[:id])
    @track = Track.find(track_params[:id])
    respond_to do |format|
      format.html { redirect_to @playlist, notice: 'Track was successfully added to the playlist.' }
      format.json { render :show, status: :ok, location: @track }
    end
  end

  def create_new_track
    @track = Track.new(track_params)

    respond_to do |format|
      if @track.save
        format.html { redirect_to @track, notice: 'Track was successfully created.' }
        format.json { render :show, status: :created, location: @track }
      else
        format.html { render :new }
        format.json { render json: @track.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tracks/1
  # PATCH/PUT /tracks/1.json
  def update
    if !@track
      respond_to do |format|
        format.html { redirect_to tracks_url, notice: 'Track was not found.' }
        format.json { head :not_found }
      end
    else
      respond_to do |format|
        if @track.update(track_params)
          format.html { redirect_to @track, notice: 'Track was successfully updated.' }
          format.json { render :show, status: :ok, location: @track }
        else
          format.html { render :edit }
          format.json { render json: @track.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE (/playlists/:playlist_id)/tracks/1
  # DELETE (/playlists/:playlist_id)/tracks/1.json
  def destroy
    if !@track
      respond_to do |format|
        format.html { redirect_to tracks_url, notice: 'Track was not found.' }
        format.json { head :not_found }
      end
    else
      if @playlist
        @playlist.tracks.delete(@track)
      else
        @track.destroy
      end

      respond_to do |format|
        format.html { redirect_to tracks_url, notice: 'Track was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_track
      if Track.exists?(params[:id])
        @track = Track.find(params[:id])
      end
    end

    def authenticate
      authenticate_or_request_with_http_basic do |name, password|
        if @playlist
          User.exists?(:username => name) && User.find_by_username(name).password == password &&
          (User.find_by_username(name).playlists.exists?(@playlist) ||
          User.find_by_username(name).playlist_shares.exists?(@playlist))
        else
          User.exists?(:username => name) && User.find_by_username(name).password == password
        end
      end
    end

    def set_playlist
      if params[:playlist_id] 
        @playlist = Playlist.find(params[:playlist_id])
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def track_params
      params.require(:track).permit(:id, :title, :artist_name)
    end
end
