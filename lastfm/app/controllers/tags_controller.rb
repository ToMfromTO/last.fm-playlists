class TagsController < ApplicationController
  before_action :set_tag, only: [:show, :edit, :update, :destroy]
  before_action :set_track, only: [:create, :destroy, :index]
  before_action :authenticate, except: [:index, :show]
  skip_before_action :verify_authenticity_token
  # wrap_parameters format: [:json]


  # GET (tracks/:track_id)/tags
  # GET (tracks/:track_id)/tags.json
  def index
    @tags = Tag.all
    if @track
      @tags = Tag.includes(:tracks).where(tracks: {id: @track.id})
    end
  end

  # GET /tags/1
  # GET /tags/1.json
  def show
  end

  # POST (tracks/:track_id)/tags
  # POST (tracks/track_id)/tags.json
  def create
    if @track
      if tag_params[:id]
        add_tag_to_track
      else
        respond_to do |format|
          format.html { redirect_to @track, notice: 'Failed to add the tag to the track. You must specify id of the tag.' }
          format.json { render json: @track, status: :unprocessable_entity }
        end
      end
    else
      create_new_tag
    end
  end

  def add_tag_to_track
    @tag = Tag.find_by id: tag_params[:id]
    @track.tags << @tag
    respond_to do |format|
      format.html { redirect_to @playlist, notice: 'Track was successfully added to the playlist.' }
      format.json { render :show, status: :ok, location: @tag }
    end
  end

  def create_new_tag
    @tag = Tag.find_by name: tag_params[:name]
    if @tag
      respond_to do |format|
          format.html { redirect_to @tag, notice: 'Tag already exists.' }
          format.json { render :show, status: :found, location: @tag }
      end
    else
      @tag = Tag.new(tag_params)
      respond_to do |format|
        if @tag.save
          format.html { redirect_to @tag, notice: 'Tag was successfully created.' }
          format.json { render :show, status: :created, location: @tag }
        else
          format.html { render :new }
          format.json { render json: @tag.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /tags/1
  # PATCH/PUT /tags/1.json
  def update
    if !@tag
      respond_to do |format|
        format.html { redirect_to tracks_url, notice: 'Tag was not found.' }
        format.json { head :not_found }
      end
    else
      respond_to do |format|
        if @tag.update(tag_params)
          format.html { redirect_to @tag, notice: 'Tag was successfully updated.' }
          format.json { render :show, status: :ok, location: @tag }
        else
          format.html { render :edit }
          format.json { render json: @tag.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE (tracks/:track_id)/tags/1
  # DELETE (tracks/:track_id)/tags/1.json
  def destroy
    if !@tag
      respond_to do |format|
        format.html { redirect_to tracks_url, notice: 'Tag was not found.' }
        format.json { head :not_found }
      end
    else
      if @track
        @track.tags.delete(@tag)
      else 
        @tag.destroy
      end

      respond_to do |format|
        format.html { redirect_to tags_url, notice: 'Tag was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tag
      if Tag.exists?(params[:id])
        @tag = Tag.find(params[:id])
      end
    end

    def set_track
      if params[:track_id] 
        @track = Track.find(params[:track_id])
      end
    end

    def authenticate
      authenticate_or_request_with_http_basic do |name, password|
        User.exists?(:username => name) && User.find_by(username: name).password == password
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tag_params
      params.require(:tag).permit(:id, :name)
    end
end
