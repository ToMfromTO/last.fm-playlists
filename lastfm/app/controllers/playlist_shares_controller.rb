class PlaylistSharesController < ApplicationController
  before_action :set_playlist_share, only: [:destroy]
  before_action :set_playlist, only: [:create]
  skip_before_action :verify_authenticity_token
  before_action :authenticate, except: [:index]

  # GET /playlists/7/shares.json
  def index
    # @playlist_shares = PlaylistShare.all
    @playlist_shares = PlaylistShare.where(playlist_id: params[:playlist_id])
  end

  # POST /playlists/7/shares.json
  def create
    @playlist_share = PlaylistShare.new(user_from_id: params[:user_from_id], user_to_id: params[:user_to_id], playlist_id: params[:playlist_id])

    respond_to do |format|
      if @playlist_share.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /playlists/7/shares/1.json
  def destroy
    @playlist_share.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_playlist_share
      @playlist_share = PlaylistShare.find(params[:id])
    end

    def authenticate
      authenticate_or_request_with_http_basic do |name, password|
        if @playlist
          User.exists?(:username => name) &&
          User.find_by_username(name).password == password &&
          User.find_by_username(name).playlists.exists?(@playlist) &&
          User.find_by_username(name).id == params[:user_from_id].to_i
        elsif @playlist_share
          User.exists?(:username => name) && User.find_by_username(name).password == password &&
          (User.find_by_username(name).id == @playlist_share.user_from.id ||
          User.find_by_username(name).id == @playlist_share.user_to.id)
        end
      end
    end

    def set_playlist
      if params[:playlist_id]
        @playlist = Playlist.find(params[:playlist_id])
      end
    end
end
