class PlaylistsController < ApplicationController
  before_action :set_playlist, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token
  wrap_parameters format: [:json]
  before_action :authenticate, except: [:index, :show]

  # GET /playlists
  # GET /playlists.json
  def index
    @playlists = Playlist.all

    # playlists = []
    # @playlists.each do |p|
    #   newp = {
    #     title:p.title,
    #     desc:p.description,
    #     owner:{
    #       id:p.user.id,
    #       username:p.user.username
    #     }
    #   }
    #   playlists.push(newp)
    # end

    # render :json => {playlist:playlists}

  end

  # GET /playlists/1
  # GET /playlists/1.json
  def show

  end

  # GET /playlists/new
  def new
    @playlist = Playlist.new
  end

  # GET /playlists/1/edit
  def edit
  end

  # POST /playlists
  # POST /playlists.json
  def create
    @playlist = Playlist.new(playlist_params)

    respond_to do |format|
      if @playlist.save
        format.html { redirect_to @playlist, notice: 'Playlist was successfully created.' }
        format.json { render :show, status: :created, location: @playlist }
      else
        format.html { render :new }
        format.json { render json: @playlist.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /playlists/1
  # PATCH/PUT /playlists/1.json
  def update
    respond_to do |format|
      if @playlist.update(playlist_update_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /playlists/1
  # DELETE /playlists/1.json
  def destroy
    @playlist.destroy
    respond_to do |format|
      format.html { redirect_to playlists_url, notice: 'Playlist was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_playlist
      @playlist = Playlist.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def playlist_params
      params.require(:playlist).permit(:title, :user_id, :description)
    end

    def playlist_update_params
      params.require(:playlist).permit(:title, :description)
    end

    def authenticate
      authenticate_or_request_with_http_basic do |name, password|
        if @playlist
          User.exists?(:username => name) &&
          User.find_by_username(name).password == password &&
          User.find_by_username(name).playlists.exists?(@playlist)
        else
          User.exists?(:username => name) &&
          User.find_by_username(name).password == password &&
          User.find_by_username(name).id == params[:user_id]

        end
      end
    end
end
