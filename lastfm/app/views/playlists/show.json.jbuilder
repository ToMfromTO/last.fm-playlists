json.extract! @playlist, :id, :title, :description

actions = [ { :rel => 'list tracks', :method => 'GET', :href => playlist_url(@playlist)+'/tracks' },
            { :rel => 'add a track', :method => 'POST', :href => playlist_url(@playlist)+'/tracks' },
            { :rel => 'list shares', :method => 'GET', :href => playlist_url(@playlist)+'/shares' },
            { :rel => 'share playlist', :method => 'POST', :href => playlist_url(@playlist)+'/shares' }, ]

json.links do 
    json.self do
        json.href playlist_url(@playlist)
    end
    json.actions(actions) do | action |
        json.extract! action, :rel, :method, :href
    end
end