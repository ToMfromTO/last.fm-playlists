json.array!(@tags) do |tag|
  json.extract! tag, :id, :name
  json.links do 
    json.self do
        json.href tag_url(tag)
    end
  end
end
