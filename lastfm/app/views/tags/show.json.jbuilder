json.extract! @tag, :id, :name, :created_at, :updated_at
json.links do 
    json.self do
        json.href tag_url(@tag)
    end
end