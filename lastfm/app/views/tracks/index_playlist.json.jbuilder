json.array!(@playlist.playlist_tracks) do |playlist_track|
  actions = [ { :rel => 'list tags', :method => 'GET', :href => track_url(playlist_track.track)+'/tags' },
              { :rel => 'add a tag', :method => 'POST', :href => track_url(playlist_track.track)+'/tags' }, ]
  json.extract! playlist_track, :order, :track
  json.links do 
      json.self do
          json.href track_url(playlist_track.track)
      end
      json.actions(actions) do | action |
          json.extract! action, :rel, :method, :href
      end
    end
end