json.array!(@tracks) do |track|
  actions = [ { :rel => 'list tags', :method => 'GET', :href => track_url(track)+'/tags' },
              { :rel => 'add a tag', :method => 'POST', :href => track_url(track)+'/tags' }, ]

  json.extract! track, :id, :title, :artist_name
    json.links do 
      json.self do
          json.href track_url(track)
      end
      json.actions(actions) do | action |
          json.extract! action, :rel, :method, :href
      end
    end
end