#TODO

- Slim down unnecessary repetition by using resourceTypes

- Links in all the schemas - Tomas ✔
- Finish methods
	- user - Gaby ✔
		- get ✔
		- create ✔
		- update ✔
		- delete ✔

		- playlists ✔
			- update ✔
			- delete ✔

			tracks ✔
				- create ✔
				- delete ✔
				
	- track - Tomas ✔
		- create ✔		
		- update ✔
		- delete ✔

	- tag - Marion ✔
		- create ✔
		- update ✔
		- delete ✔
	
- Generate the API documentation from the RAML spec - Max
- Add generated documentation to the documentation document we already have - Max
- Create presentation slides
